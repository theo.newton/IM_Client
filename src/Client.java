import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/*
 * Created by Theo Newton 27th December 2016
 */

public class Client extends JFrame{
	private final int portNum = 8008;
	private JTextField userText;
	private JTextArea chatWindow;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private String msg = "";
	private String serverIP = "127.0.0.1";
	private Socket connection;

	/*
	 * Constructor method
	 * @param host	The host server
	 */
	public Client(String host){
		super("Scamander Chat Client");
		serverIP = host;
		userText = new JTextField();
		userText.setEditable(false);
		userText.addActionListener(
				new ActionListener(){
					public void actionPerformed(ActionEvent event){
						sendMessage(event.getActionCommand());
						userText.setText("");
					}
				}
		);
		add(userText, BorderLayout.NORTH);
		chatWindow = new JTextArea();
		add(new JScrollPane(chatWindow), BorderLayout.CENTER);
		setSize(500, 250);
		setVisible(true);
	}

	public void run(){
		try{
				connectToServer();
				setupStreams();
				chatHandler();
		}
		catch(EOFException e){
			showMessage("\n Client terminated connection");
		}
		catch(IOException e){
			// Something went very very wrong. Oops.
			e.printStackTrace();
		}
		finally{
			closeAndTidy();
		}
	}
	/*
	*	 Form connection with server
	*/
	public void connectToServer() throws IOException{
		showMessage("Attempting to connect \n");
		connection = new Socket(InetAddress.getByName(serverIP), portNum);
    	showMessage("Connected to " + connection.getInetAddress().getHostName());
	}
	/*
	*	Setup streams for sending and recieving messages
	*/
	public void setupStreams() throws IOException{
		output = new ObjectOutputStream(connection.getOutputStream());
		output.flush();
		input = new ObjectInputStream(connection.getInputStream());
		showMessage("\n Streams have been successfully set up");
	}

	public void chatHandler() throws IOException{
		String msg = "You are now connected";
		sendMessage(msg);
		allowMsgText(true);
		do{
			try{
				msg = (String) input.readObject();
				showMessage("\n" + msg);
			}catch(ClassNotFoundException e){
					showMessage("\n ERROR: Invalid message data");
			}
		}
		while(!msg.equals("SERVER - END"));
	}

	/*
	 *  Allow the user to type text
	 *  @param isAllowed	Change whether the user has permission to type or not
	 */
	private void allowMsgText(final boolean isAllowed){
		SwingUtilities.invokeLater(
				// Create our thread
				new Runnable(){
					public void run(){
						userText.setEditable(isAllowed);
					}
				}
		);
	}

	/*
	 * 	Close streams and sockets
	 */
	private void closeAndTidy(){
		showMessage("\n Terminating connections \n");
		allowMsgText(false);
		try{
			output.close();
			input.close();
			connection.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}

	/*
	 * 	Send a message to the client
	 * 	@param	msg	The string that the server wishes to send to the client
	 */
	private void sendMessage(String msg){
		try{
			output.writeObject("CLIENT - " + msg);
			output.flush();
			showMessage("\n CLIENT - " + msg);
		}
		catch(IOException e){
			chatWindow.append("\n ERROR: Message failed to send \n");
		}

	}

	/*
	 * 	The text that will be displayed in the chat window
	 * 	@param	msgText	The string of text to display on the chat client
	 */
	private void showMessage(final String msgText){
		SwingUtilities.invokeLater(
				// Create our thread
				new Runnable(){
					public void run(){
						chatWindow.append(msgText);
					}
				}
		);
	}



}
